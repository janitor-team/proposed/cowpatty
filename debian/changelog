cowpatty (4.8-3) unstable; urgency=medium

  [ Samuel Henrique ]
  * Bump DH to 13
  * Bump Standards-Version to 4.5.0
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian
  * d/control: Add Rules-Requires-Root: no

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Samuel Henrique <samueloph@debian.org>  Sat, 12 Sep 2020 22:33:02 +0100

cowpatty (4.8-2) unstable; urgency=medium

  * update project url (closes: #909516)
  * Bump Standards-Version to 4.2.1
  * d/patches:
    - makefile-parallel.patch: new patch to fix parallel builds FTBFS
    - kali-overflow.patch: Add Forwarded tag
  * d/rules: remove trailing whitespace
  * d/watch: point to github

 -- Samuel Henrique <samueloph@debian.org>  Wed, 03 Oct 2018 03:57:25 -0300

cowpatty (4.8-1) unstable; urgency=medium

  * Initial Debian release (closes: #841472)
  * New upstream version 4.8
  * Bump DH level to 11
  * Bump Watch to v4
  * Bump Standards-Version to 4.1.5
  * d/control
    - from kali to debian under pkg-sec team
    - Priority: from extra to optional
  * d/copyright:
    - delete problematic blank line
    - remove double declaration of GPL-2+
    - reflect upstream change to BSD3 and update debian/*
  * d/cowpatty.manpages: use upstream manpages
  * d/patches:
    - makefile-flags.patch: new patch to fix hardening and CC flag
      (was using clang)
    - fix-makefile.patch: update patch
    - cowpatty-4.6-fixup16-patch:
      ~ refresh and update patch to 4.8
      ~ rename to kali-overflow.patch
      ~ update DEP-3 headers and add Forwarded field
  * d/rules: export hardening=+all
  * d/lintian-overrides: no gpl code linked with openssl
  * wrap-and-sort -a

 -- Samuel Henrique <samueloph@debian.org>  Thu, 05 Jul 2018 23:09:34 -0300

cowpatty (4.6-1kali3) kali-dev; urgency=medium

  * Do not hardcode dependency on C libraries.
  * Modernise the packaging.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 24 Nov 2015 18:30:15 +0100

cowpatty (4.6-1kali2) kali; urgency=low

  * Added watch file (Closes: 0000886)

 -- Devon Kearns <dookie@kali.org>  Thu, 09 Jan 2014 13:50:55 -0700

cowpatty (4.6-1kali1) kali; urgency=low

  * Added cowpatty-4.6-fixup16-patch

 -- Mati Aharoni <muts@kali.org>  Sun, 11 Aug 2013 18:12:48 -0400

cowpatty (4.6-1kali0) kali; urgency=low

  * Upstream import

 -- Mati Aharoni <muts@kali.org>  Sun, 11 Aug 2013 18:03:09 -0400

cowpatty (4.3-1kali3) kali; urgency=low

  * Removed desktop file

 -- Mati Aharoni <muts@kali.org>  Fri, 14 Dec 2012 20:22:47 -0500

cowpatty (4.3-1kali2) kali; urgency=low

  * Fixed build

 -- dookie <dookie@kali.org>  Fri, 09 Nov 2012 15:40:07 -0700

cowpatty (4.3-1kali1) kali; urgency=low

  * Initial release

 -- dookie <dookie@kali.org>  Fri, 09 Nov 2012 15:09:59 -0700
